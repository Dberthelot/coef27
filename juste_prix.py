import easygui
from easygui import *
from random import randint
easygui.codebox ("Les regles du jeu sont simples : La machine choisit un entier sur une intervalle selon votre niveau de difficulté puis vous avez 5 essais pour arriver à découvrir de quelle entier il s'agit grâce aux indications que la machine vous procure.")
nombre_essais_possible = 5

niveau = easygui.buttonbox ("Choisir votre niveau de difficulté", choices = ["Facile (entier entre 1 et 30)","Normal (entier entre 1 et 50)","Difficile (entier entre 1 et 100)"])
if niveau == "Facile (entier entre 1 et 30)":
    nombre_a_deviner = randint(1, 30)
elif niveau == "Normal (entier entre 1 et 50)":
    nombre_a_deviner = randint(1,50)
elif niveau == "Difficile (entier entre 1 et 100)":
    nombre_a_deviner = randint(1,100)


compteur = 0
while compteur < nombre_essais_possible :

	essai = easygui.integerbox('Entrez un nombre ({0}e  essai): '.format(compteur+1))

	if essai < nombre_a_deviner:
		easygui.msgbox('Le nombre a deviner est plus grand que {0}'.format(essai))
	elif essai > nombre_a_deviner:
		easygui.msgbox('Le nombre a deviner est plus petit que {0}'.format(essai))
	elif essai == nombre_a_deviner:
		easygui.msgbox('Bravo vous avez gagne en {0} essai(s)'.format(compteur + 1))
		break

	compteur += 1

if essai != nombre_a_deviner:
	easygui.msgbox('Vous avez perdu !')
	easygui.msgbox('Le nombre a deviner etait: {0}'.format(nombre_a_deviner))


