MON JEU 

Bien qu'il ne soit peu original mon jeu est reutilisable en effet contrairement au quizz la réponse change à toutes les parties car il s'agit du juste prix.

EXPLICATION 

Le jeu du juste prix est simple, tout d'abord l'ordinateur rentre un entier compris entre 1 et 100 si le joueur choisit la difficulté la plus haute, entre 1 et 50 si il choisit la diificulté normal et entre 1 et 30 si il choisit la dificulté facile.
Ensuite le joueur possède 5 essais pour trouver ce nombre grâce aux indications de la machine qui lui indique si le chiffre choisi par le joueur est trop grand ou trop petit.
Si le joueur arrive a trouver le bon chiffre une fenêtre apparait et indique au joueur qu'il a gagné en (x) essais(s)
Autrement si le joueur ne parvient pas a trouver le bon nombre, une fenêtre apparait et indique au joueur qu'il a perdu et lui montre qu'elle était le nombre à deviner.

EXPLICATION EN DETAIL

1- Tout d'abord on importe Easygui ce qui va nous permettre de créer une interface graphique

2- Ensuite d'easygui nous importons toutes ces fonctions avec *

3- De la bibliothèque Random nous importons randint un outil qui permet a la machine de ressortir un entier aléatoire sur un intervalle défini.

4- Avec  easygui.codebox nous créons les règles du jeu

5- Puis nous fixons le nombre d'essais possibles sur 5

6- Après on définit la variable niveau avec un buttonbox qui possède 3 choix ce qui correspond aux 3 niveaux de difficulté avec l'intervalle inscrit a côté

7- Avec IF et ELIF ont indique que si la variable niveau est égal a un des choix il prend l'intervalle de ce choix 

8- On initialise un compteur sur 0

9- Ensuite on crée une boucle while (compteur < nombre_essais_possible ) sachant que nombre_essais_possible est égale a 5 celle ci permet au indications de la boucle de se stopper une fois que les 5 essais sont passés

10- On définit la variable essai avec un integerbox cela permet a l'utilisateur d'entrer un nombre dans la fenetre puis on précise a quelle essais sommes nous rendu avec la fonction format pécedé d'un point qui correspond a l'assignation de la variable compteur. Format permet de remplacer le {0} par la valeur de la variable compteur soit 0+1 pour le premier essais en effet on rajoute +1 car il ne peut avoir d'essais 0


11- avec ELIF et IF ont fixe des conditions pour donner des indications sur la valeur d'essai en effet ->

si l'essai est plus grand que le nombre a deviner on indique au joueur avec un msgbox que le nombre a deviner est plus petit que l'essai avec la fonction Format qui remplace {0} par l'essai
sinon si l'essai est plus petit que le nombre a deviner on indique au joueur avec un msgbox que le nombre a deviner est plus grand que l'essai encore avec la fonction Format qui remplace {0} par l'essai
pour finir si l'essai est égale au nombre à deviner on indique au joueur toujours avec un msgbox Bravo vous avez gagne en {0} essai(s) et le {0} est remplacé par le compteur + 1 avec une nouvelle fois la fonction format 

12- Une fois que les 5 essais sont passés soit on a trouvé le nombre a deviner et alors le programme s'arrete avec la condition break

13- Sinon si l'essai est différent ( != ) du nombre a deviner avec msgbox on affiche vous avez perdu ! puis avec un autre msgbox on affiche le nombre qui était a deviner avec Format qui remplace {0} par nombre_a_deviner

VIDEO EXEMPLE

https://mega.nz/#!nIAFkCqa!IPg3T7bNRm3tsm1Nz_BjO7O_qNd_zSAnFKLZQFwLZ70